import java.util.ArrayList;

import java.util.Scanner;
public class List {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n,e,small=999,larg=-9999,pal,prime,aux,cop=0;
		System.out.println("Enter the number of elements: ");
		n=sc.nextInt();
		
	ArrayList<Integer> list=new ArrayList<Integer>(n);
	ArrayList<Integer> palin=new ArrayList<Integer>(n);
	ArrayList<Integer> prim=new ArrayList<Integer>(n);
	while(n!=0)
	{
		System.out.println("Element is: ");
		e=sc.nextInt();
		list.add(e);
		n--;
	}
	
	for(Integer x1: list)
	{
		if(small>x1)
		{
			small=x1;
		}
	}
	for(Integer x2: list)
	{
		if(larg<x2)
			larg=x2;
	}
	for(Integer x4: list)
	{
		cop=0;
		aux=x4;
		while(aux!=0)
		{
			cop=cop*10+aux%10;
			aux=aux/10;
		}
		if(cop==x4)
		{
			palin.add(x4);
		}
			
	}
	for(Integer x5:list)
	{
		int c=0;
		aux=x5;
		for(int i=2;i<aux/2;i++)
		{
			if(aux%i==0)
			{
				c++;
			}
		}
		if(c<1)
		{
			prim.add(aux);
		}
				
	}
	System.out.println("Smallest number is: " + small);
	System.out.println("Largest number is: " + larg);
	System.out.println("Palindrom: ");
	for(Integer x6:palin)
	{
		System.out.println(x6);
	}
	System.out.println("Prime numbers is: ");
	for(Integer x7:prim)
		System.out.println(x7);
	
	

	}

}
